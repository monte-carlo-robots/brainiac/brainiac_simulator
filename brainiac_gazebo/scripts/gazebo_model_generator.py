#!/usr/bin/python
import os
import yaml
import numpy as np
import xmltodict

from PIL import Image
import matplotlib.pyplot as plt
from matplotlib.table import Table

file_path = os.path.dirname(os.path.abspath(__file__))
package_path = os.path.dirname(os.path.normpath(file_path))


def generate_plot(numpy_matrix, bkg_colors=None, dim_size=None):
    """
    Generate the figure for the grid map
    @param numpy_matrix: numpy matrix with the grid map representation data
    @param bkg_colors: colors map for the grid cells
    @return: figure object for plotting and saving an image for the grid map representation
    """
    if bkg_colors is None:
        bkg_colors = ['white', 'black']
    fig, ax = plt.subplots(figsize=(10, 10), dpi=1, edgecolor='red')
    # plot configurations for setting either no padding and margin
    plt.margins(0, 0)
    plt.gca().xaxis.set_major_locator(plt.NullLocator())
    plt.gca().yaxis.set_major_locator(plt.NullLocator())
    ax.set_axis_off()
    tb = Table(ax, bbox=[0, 0, 1, 1])
    num_rows, num_cols = numpy_matrix.shape
    width, height = 1.0 / num_cols, 1.0 / num_rows
    cell_label = ""
    # Add cells
    for (i, j), val in np.ndenumerate(numpy_matrix):
        # Index either the first or second item of bkg_colors based on a checker board pattern
        color = bkg_colors[val]
        tb.add_cell(i, j, text=cell_label, edgecolor='black', loc='center', facecolor=color)
        cell = tb.get_celld()[(i, j)]
        cell.set_linewidth(0.15)
        cell.set_width(10)
        cell.set_height(10)
    ax.add_table(tb)
    fig.tight_layout(pad=0)
    return fig


def get_image(numpy_matrix, model='occupancy_grid_ground', scale_factor=100,
              out_file_name='occupancy_grid_ground', out_file_type='png'):
    """
    Generate the figure for the grid map
    @type out_file_type: object
    @param scale_factor: scale factor
    @param model:
    @param out_file_name:
    @param numpy_matrix: numpy matrix with the grid map representation data
    @param bkg_colors: colors map for the grid cells
    @return: figure object for plotting and saving an image for the grid map representation
    """
    numpy_matrix *= 255
    img_origin = Image.fromarray(numpy_matrix.astype(np.uint8))
    img_origin.save("{0}.{1}".format(os.path.join(package_path, 'maps', out_file_name), out_file_type))
    map_matrix = np.kron(numpy_matrix, np.ones((scale_factor, scale_factor)))
    img_scale = Image.fromarray(map_matrix.astype(np.uint8))
    complete_file_name = os.path.join(package_path, 'models', model, "materials/textures", out_file_name)
    img_scale.save("{0}.{1}".format(complete_file_name, out_file_type))


def load_yaml(complete_file_path):
    """
    Load data from yaml file
    @param complete_file_path: file from for yaml file
    @return: return data in dict format
    """
    with open(complete_file_path, 'r') as file:
        yaml_data = yaml.load(file, Loader=yaml.FullLoader)
        return yaml_data


def generate_map(yaml_path='/config/grid.yaml', scale=100, random_shape=None):
    """
    Generates the map with input from the
    @param random_shape: shape for random map
    @param yaml_path: yaml_path definition for the occupancy grid map
    @param scale: scale for matrix size in image
    """
    if random_shape:
        numpy_array_random = np.random.choice([0, 1], size=random_shape, p=[1. / 2, 1. / 2])
        get_image(numpy_array_random, scale_factor=scale)
    else:
        yaml_path = os.path.dirname(__file__).replace('/scripts', yaml_path)
        yaml_data = load_yaml(yaml_path)
        get_image(np.array(yaml_data['grid_array']))


def resize_sdf_model(sdf_path=None, size=(1, 1), scale=10):
    """
    Function to resize the image background
    @param sdf_path: model.sdf file path
    @param size: initial size of the background model
    @param scale: scale factor for increasing dimensions
    """
    if sdf_path is None:
        sdf_path = os.path.join(package_path, 'models', 'occupancy_grid_ground',
                                'model.sdf')
    file_descriptor = open(sdf_path, 'r')
    file = file_descriptor.read()
    file_descriptor.close()
    sdf_model = xmltodict.parse(file)
    sdf_model_link = sdf_model["sdf"]["model"]["link"]
    sdf_collision_size = sdf_model_link["collision"]["geometry"]["plane"]
    sdf_visual_size = sdf_model_link["visual"]["geometry"]["plane"]
    sdf_collision_size["size"] = "{0} {1}".format(size[0] * scale, size[1] * scale)
    sdf_visual_size["size"] = "{0} {1}".format(size[0] * scale, size[1] * scale)
    write_file = open(sdf_path, 'w')
    write_file.write(xmltodict.unparse(sdf_model))
    write_file.close()


if __name__ == '__main__':
    generate_map(random_shape=(20, 20), scale=300)
    resize_sdf_model(size=(10, 10), scale=0.1)
