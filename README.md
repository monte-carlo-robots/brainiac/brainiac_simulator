# Brainiac Robot 
<img src="https://i.pinimg.com/originals/20/d1/7c/20d17cccb0ef5f75ddc5b8a79d30cc56.png" width="200"><br>


Brainiac is a low-cost robot developed with the open-source robotic operating system ROS. 

Brainiac was created by Henrique José Ferreira and Vinícius Ribeiro in June 2020. 
The main purpose of Brainiac is to give students and researches a low-cost hardware and software project implementation to test and develop 
robotics localization and search algorithms such as particle filter and kalman filter.

The Brainiac Robot consists of a mobile base, infrared distance sensor, 2 motors with rotary encoders and reductions, IMU sensor, raspberry and beaglebong board. 

## Getting Started

This repository relates to the Brainiac Simulator package wich was developed with ROS and the Gazebo physical enviroment simulator, this implementation enhaces the Brainiac project and gives the power to test algorithms in a virtual enviroment before deploy and test then in the real world.

This repository is a metapackage with inner packages:
- [brainiac_description](https://gitlab.com/monte-carlo-robots/brainiac/brainiac_simulator/-/tree/master/brainiac_description)
- [brainiac_simulator](https://gitlab.com/monte-carlo-robots/brainiac/brainiac_simulator/-/tree/master/brainiac_simulator)

### Intallation Requirements

This package was developed with C++ 11, Python 3.x, ROS and Gazebo Framework. 
See the link below for more information about the installation process.
 
- [ROS Framework - (Kinect Version)](http://wiki.ros.org/kinetic/Installation/Ubuntu)
- [Gazebo Framework - (7.x Version)](http://gazebosim.org/tutorials?tut=install_ubuntu)
